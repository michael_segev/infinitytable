#ifndef __gameMana_hh
#define __gameMana_hh
#include "main.h"
#include "Game.hh"

typedef enum
{
	GAME_LASER = 0,
	GAME_TUG_OF_WAR,
	GAME_FFT,
	GAME_RAINBOW,
	GAME_NUM_GAMES,
	GAME_NULL
}GameName;


class GameManager
{
	private:
		GameName activeGameName;
		Game * pActiveGame;
  public:
		GameManager(void);
		void run(void);
};
#endif
