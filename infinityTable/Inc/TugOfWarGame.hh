#ifndef __tugofwar_game_hh
#define __tugofwar_game_hh
#include "main.h"
#include "Game.hh"

typedef enum
{
	START,
	COLLISION_EFFECT,
	TUG_OF_WAR,
	VICTORY
}TugOfWarState;


class TugOfWarGame: public Game
{
	private:
		bool gameStarted;
		TugOfWarState state;
		int32_t numButtonTaps[2]; //stores number of times each player hit the button since start of game
		int16_t startIdx[2];
		uint32_t timestamp;
  public:
		TugOfWarGame(void);
		virtual void startGame(void);
		virtual void stopGame(void);
		virtual void runGameEngine(void);
};
#endif
