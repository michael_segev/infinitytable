#ifndef __sprite_h
#define __sprite_h
#include "main.h"
#include "Pixel.hh"
#include <vector>

class Sprite 
{
	private:
		std::vector<Pixel> pixelsVector;
		int16_t locationIdx;
		float locationIdxFloat;
		uint8_t priority;
		float velocity;
		bool tailFade;
		bool overflowMode;		//if set to true pixels out of strip bounds will overflow to the other end of the strip
	
  public:
		Sprite(void);
		Sprite(uint16_t numPixels);
		Sprite(uint16_t numPixels, uint8_t r, uint8_t g, uint8_t b, int16_t locIdx, uint8_t prio, float velo);
		Sprite(uint16_t numPixels, uint8_t r, uint8_t g, uint8_t b, int16_t locIdx, uint8_t prio, float velo, bool tail);
		void addPixels(uint16_t numPixels);
		void setColor(uint8_t r, uint8_t g, uint8_t b);
		void getColor(uint8_t &r, uint8_t &g, uint8_t &b);
		void setLocationIdx(float location);
		float getLocationIdx(void);		
		float getHeadLocationIdx(void);
		void setPriority(uint8_t prio);
		void setVelocity(float velo);
		float getVelocity(void);
		uint32_t getNumPixels(void);
		void setSpriteInStrip(void);
		void setOverflowMode(bool value);
};
#endif
