#ifndef BUTTONS_H
#define BUTTONS_H
#ifdef __cplusplus
 extern "C" {
#endif
#include "main.h"
#include "stdbool.h"

//void pollKnobValues(void);
void setKnobValue(uint8_t knobIndex, uint32_t value);
float getKnobPosition(uint8_t idx); 
bool getArcadeButtonValue(uint8_t idx);	 
	 
#ifdef __cplusplus
}
#endif
#endif	 
