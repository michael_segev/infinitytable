#ifndef LED_DRV_H
#define LED_DRV_H
#ifdef __cplusplus
 extern "C" {
#endif
#include "main.h"
#include "tim.h"

typedef struct LEDpixel_tdef
{
	uint8_t r;
	uint8_t g;
	uint8_t b;
	uint8_t priority;
} LEDpixel_tdef;
	
void initLedStrip(void);
void sendRGBtoStrip(uint8_t r, uint8_t g, uint8_t b);
void latchLedStrip(void);
void sendPixelsToStrip(void);
void sendPixelsToStrip_IT_Timer(void);
void sendPixelsToStrip_IT(void);
uint8_t getPixelPriorityByIndex(int16_t idx);
LEDpixel_tdef getPixelByIndex(int16_t idx);
void setPixelByIndex(uint8_t r, uint8_t g, uint8_t b, int16_t idx, uint8_t prio);
void clearPixels(void);
uint16_t LedDriver_GetNumLEDS(void);
void LedDriver_SetZeroIndexOffset(uint16_t offset);
#ifdef __cplusplus
}
#endif
#endif
