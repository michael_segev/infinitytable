#ifndef __game_hh
#define __game_hh
#include "main.h"
#include "Sprite.hh"
#include "SoundEngine.hh"
#include <vector>

extern SoundEngine 		soundEngine; //defined in main.cpp

class Game 
{
	private:
		
  public:
		std::vector<Sprite> SpritesVector;
	  std::vector<Pixel> PixelsVector;
		Game(void);
		void addSprite(Sprite spr);
		void removeSprite(uint32_t idx);
		void removeAllSprites(void);
		void addPixel(Pixel pixel);
		void removePixel(uint32_t idx);
		void removeAllPixels(void);	
		void setGameInStrip(void);
		virtual void runGameEngine(void);
		virtual void startGame(void);
		virtual void stopGame(void);	
};
#endif
