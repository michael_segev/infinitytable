#ifndef __fftgame_game_hh
#define __fftgame_game_hh
#include "main.h"
#include "Game.hh"


class FFTGame: public Game
{
	private:
		bool gameStarted;
  public:		
		FFTGame(void);
		virtual void startGame(void);
		virtual void stopGame(void);
		virtual void runGameEngine(void);
};
#endif
