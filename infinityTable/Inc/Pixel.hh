#ifndef __pixel_HH
#define __pixel_HH
#include "main.h"

class Pixel 
{
	private:
		int16_t locationIdx;
		uint8_t red,green,blue;
		uint8_t priority;
		bool overflowMode;
  public:
		Pixel(void);
		Pixel(int16_t Idx);
		Pixel(int16_t Idx, uint8_t r, uint8_t g, uint8_t b);
		void setColor(uint8_t r, uint8_t g, uint8_t b);
		void setLocationIdx(int16_t locationIdx);
		void getColor(uint8_t &r, uint8_t &g, uint8_t &b);
		int16_t getLocationIdx(void);
		uint8_t getPriority(void);
		void setPriority(uint8_t prio);
		void setOverflowMode(bool value);
		void setPixelInStrip(void);
};
#endif
