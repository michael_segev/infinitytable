#ifndef BUZZ_DRV_H
#define BUZZ_DRV_H
#ifdef __cplusplus
 extern "C" {
#endif
#include "main.h"
#include "tim.h"
	
void initBuzzer(void);
void setBuzzerFrequency(uint32_t fhz);
void turnOnBuzzer(void);
void turnOffBuzzer(void);

#ifdef __cplusplus
}
#endif
#endif
