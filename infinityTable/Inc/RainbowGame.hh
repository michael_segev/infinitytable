#ifndef __rainbow_game_hh
#define __rainbow_game_hh
#include "main.h"
#include "Game.hh"


class RainbowGame: public Game
{
	private:	
  public:
		RainbowGame(void);
    virtual		void runGameEngine(void);
    virtual		void startGame(void);
    virtual		void stopGame(void);	
};
#endif
