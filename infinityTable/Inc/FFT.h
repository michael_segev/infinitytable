#ifndef FFT_H
#define FFT_H
#ifdef __cplusplus
 extern "C" {
#endif
#include "main.h"
#define FFT_WINDOW_SIZE 1024
#define FFT_MAGNITUDE_SIZE FFT_WINDOW_SIZE / 2
#define SAMPLING_FREQUENCY 30628
#define BIN_FREQUENCY (SAMPLING_FREQUENCY/2)/FFT_WINDOW_SIZE	 

		void initFFT(void);
		void runFFT(float* inputSamples, float* outputMagnitudes);
		void FFTcomputeAudioMagnitudes(float* inputSamples);
		void FFTgetAudioMagnitudes(float** outputMagnitudes);
	 
#ifdef __cplusplus
}
#endif
#endif	 
