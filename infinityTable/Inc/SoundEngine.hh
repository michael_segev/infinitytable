#ifndef __sound_eng_hh
#define __sound_eng_hh
#include "main.h"

class SoundEngine
{
	private:
		bool 					activeSound;
		bool 					pendingActiveSound;
		uint32_t			activeSoundFrequency;
		uint32_t	 		activeSoundStartTimestamp;
		int32_t 			activeSoundDurationMS;
		int8_t 				activeSoundPriority;
	
  public:
		SoundEngine(void);
		bool 					setActiveSound(uint32_t frequency, int32_t durationMS, int8_t priority);
		bool					stopActiveSound(int8_t priority);
		uint32_t 			getActiveSoundFrequency(void);
		void 					runEngine(void);

};
#endif
