#ifndef __laser_game_hh
#define __laser_game_hh
#include "main.h"
#include "Game.hh"


class LaserGame: public Game
{
	private:	
  public:
		LaserGame(void);
		virtual void runGameEngine(void);
		virtual void startGame(void);
		virtual void stopGame(void);	
};
#endif
