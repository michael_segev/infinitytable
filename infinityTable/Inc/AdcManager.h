#ifndef ADCMANA_H
#define ADCMANA_H
#ifdef __cplusplus
 extern "C" {
#endif
//#include "main.h"

void AdcManager_Init(void);
void AdcManager_StartDmaAdc(void);
void AdcManager_ProcessNewAdc(void);	 
void AdcManager_DmaAdcComplete(void);
	 
#ifdef __cplusplus
}
#endif
#endif	 
