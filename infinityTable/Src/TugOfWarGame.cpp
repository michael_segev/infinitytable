#include "TugOfWarGame.hh"
#include "Sprite.hh"
#include "buttons.h"
#include <stdlib.h>
#include <vector>
#include "LedDriver.h" // to get num leds

TugOfWarGame::TugOfWarGame(void)
{
	gameStarted = false;
	numButtonTaps[0] = 0;
	numButtonTaps[1] = 0;
	state = START;
}

void TugOfWarGame::startGame(void)
{
	uint16_t fireballSize;
	float velocity = 1;
	
	Game::startGame(); //parent start game removes all sprites	
	
	gameStarted = true;	
	state = START;
	numButtonTaps[0] = 0;
	numButtonTaps[1] = 0;	
	timestamp = HAL_GetTick();
	fireballSize = LedDriver_GetNumLEDS() / 3;
	//player 1 fireball (backwards)
	addSprite(Sprite(fireballSize , 255, rand() % 255/3, rand() % 255/3, LedDriver_GetNumLEDS(), 0, -velocity, true)); //numpixels, r, g, b, locationIdx, priority, velocity, tailFade
	//player 2 fireball (forwards)
	addSprite(Sprite(fireballSize, rand() % 255/3, rand() % 255/3, 255, -fireballSize, 0, velocity, true)); //numpixels, r, g, b, locationIdx, priority, velocity, tailFade
	
	//store starting locations of each fireball
	startIdx[0] = LedDriver_GetNumLEDS();
	startIdx[1] = -fireballSize;	
}

void TugOfWarGame::stopGame(void)
{
	Game::stopGame(); //parent stop game removes all sprites
	gameStarted = false;
}


void TugOfWarGame::runGameEngine(void)
{
	static bool prevButVals[2] = {true, true};
	bool butVals[2];
	float velocity;
	float staticVelocityRatio = 0.005f; //moves by this velocity even if you don't press anything
	float idx;
	
	butVals[0] = getArcadeButtonValue(0);
	butVals[1] = getArcadeButtonValue(1);	
	
	if(butVals[0] == false && prevButVals[0] == true) //press down
	{
		//player one button
		numButtonTaps[0]++;
	}
	
	if(butVals[1] == false && prevButVals[1] == true) //press down
	{
		numButtonTaps[1]++;
	}
	
	prevButVals[0] = butVals[0];
	prevButVals[1] = butVals[1];	
	
	switch(state)
	{
		case START:
			//start of game position gets set based on number of hits of the buttons
			velocity = SpritesVector[0].getVelocity();
			idx = startIdx[0] + numButtonTaps[0] * velocity + staticVelocityRatio*velocity*(HAL_GetTick() - timestamp);
			SpritesVector[0].setLocationIdx(idx);
		
			velocity = SpritesVector[1].getVelocity();
			idx = startIdx[1] + numButtonTaps[1] * velocity + staticVelocityRatio*velocity*(HAL_GetTick() - timestamp);
			SpritesVector[1].setLocationIdx(idx);		
		
			if(SpritesVector[0].getHeadLocationIdx() <= SpritesVector[1].getHeadLocationIdx())
			{
				state = COLLISION_EFFECT;
			}
			break;
		case COLLISION_EFFECT:
			soundEngine.setActiveSound(300,500,0);
			numButtonTaps[0] = 0;
			numButtonTaps[1] = 0;
			//store collision idx
			startIdx[0] = SpritesVector[0].getLocationIdx();
			startIdx[1] = SpritesVector[1].getLocationIdx();
			state = TUG_OF_WAR;
			break;
		case TUG_OF_WAR:
			velocity = SpritesVector[0].getVelocity();
			idx = startIdx[0] + (numButtonTaps[0] * velocity) - (numButtonTaps[1] * velocity);
			SpritesVector[0].setLocationIdx(idx);
			velocity = SpritesVector[1].getVelocity();
			idx = startIdx[1] + (numButtonTaps[1] * velocity) - (numButtonTaps[0] * velocity);		
			SpritesVector[1].setLocationIdx(idx);
			if(SpritesVector[0].getHeadLocationIdx() < 0 || SpritesVector[1].getHeadLocationIdx() > LedDriver_GetNumLEDS() - 1)
			{
				state = VICTORY;
			}
			break;
		case VICTORY:
			soundEngine.setActiveSound(1000,500,0);
			stopGame();
			startGame();
			state = START;
			break;
		default:
			break;		
	}
	
	setGameInStrip();
}
