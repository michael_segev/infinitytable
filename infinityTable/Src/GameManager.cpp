#include "GameManager.hh"
#include "buttons.h"
#include "LaserGame.hh"
#include "TugOfWarGame.hh"
#include "FFTGame.hh"
#include "RainbowGame.hh"
#include "SoundEngine.hh"

LaserGame 			laserGame;
TugOfWarGame		tugOfWarGame;
FFTGame					fftGame;
RainbowGame     rainbowGame;

#define NUM_ACTIVE_GAMES 4

Game * gamesList[NUM_ACTIVE_GAMES] = 
{
	&laserGame,
	&tugOfWarGame,
	&fftGame,
	&rainbowGame
};

GameManager::GameManager(void)
{
	//constructor
	this->activeGameName = GAME_NULL;
}

void GameManager::run(void)
{
		float knobPos0 = getKnobPosition(0);
		int gameIndex = (int)(knobPos0/100*NUM_ACTIVE_GAMES);
		if( this->activeGameName != gameIndex)
		{
				//game change!
				if(this->pActiveGame != NULL)
				{
					this->pActiveGame->stopGame(); //stop previous game
					soundEngine.setActiveSound(261,25,0);
				}
				
				this->pActiveGame = gamesList[gameIndex]; //assign pointer to active game
				this->pActiveGame->startGame(); //start current game
				this->activeGameName = (GameName)gameIndex; //set new game name					
		}
			
		//Run the game engine
		if(this->pActiveGame == NULL)
		{
			return;
		}
		this->pActiveGame->runGameEngine();	
}