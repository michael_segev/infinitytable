#include "AdcManager.h"
#include "stm32l4xx_hal.h"
#include "adc.h"
#include "FFT.h"
#include "buttons.h"

#define true 1
#define false 0
	
#define NUM_SAMPLES_PER_BUFFER 			FFT_WINDOW_SIZE * 4 //after dma completes conversion, there will be FFT_WINDOW_SIZE number of audio samples and ready to compute fft on it

volatile static uint16_t adcSampleBuffers[2][NUM_SAMPLES_PER_BUFFER];
uint8_t activeSampleBuffer = 0;
uint8_t newAdcData = 0;

void _AdcManager_RunDmaAdc(void);

void AdcManager_Init(void)
{
		
}

void AdcManager_StartDmaAdc(void)
{	
	_AdcManager_RunDmaAdc();
}


void AdcManager_ProcessNewAdc(void)
{	
	if(newAdcData)
	{
		uint8_t SampleBufferIndex = !activeSampleBuffer;
		uint16_t knobData[3]; //potentiometer values
		float audioData[NUM_SAMPLES_PER_BUFFER/4]; //data gets updated 
		int j = 0;
		int i = 0;
		
		//deinterlace data since data comes in sequentially round robin format for each channel
		//order is knob1,knob2,knob3,audio,knob1,knob2,knob3,audio etc...
		while(i < NUM_SAMPLES_PER_BUFFER)
		{
			knobData[0] = adcSampleBuffers[SampleBufferIndex][i];
			i++;
			if(i >= NUM_SAMPLES_PER_BUFFER) break;
			knobData[1] = adcSampleBuffers[SampleBufferIndex][i];
			i++;		
			if(i >= NUM_SAMPLES_PER_BUFFER) break;
			knobData[2] = adcSampleBuffers[SampleBufferIndex][i];
			i++;		
			if(i >= NUM_SAMPLES_PER_BUFFER) break;
			
//			{
//			static float audioDataLPF = 0.0f;
//			audioDataLPF = audioDataLPF*0.9f + adcSampleBuffers[SampleBufferIndex][i]*0.1;
//			audioData[j] = audioDataLPF;
//			}
			audioData[j] = adcSampleBuffers[SampleBufferIndex][i];
			j++;
			i++;
		}
		//process data
		setKnobValue(0,knobData[0]); //take newest data and set knob value
		setKnobValue(1,knobData[1]); //take newest data and set knob value
		setKnobValue(2,knobData[2]); //take newest data and set knob value
		FFTcomputeAudioMagnitudes(audioData);
		
		newAdcData = false;
	}
}

//called in interrupt context
void AdcManager_DmaAdcComplete(void)
{
	activeSampleBuffer = !activeSampleBuffer; //toggle the active sample buffer to alternate
	_AdcManager_RunDmaAdc();
	//debug catch faults
//	if(newAdcData == true)
//	{
//		while(1);
//	}
	newAdcData = true;
}

void _AdcManager_RunDmaAdc(void)
{
	HAL_ADC_Start_DMA(&hadc1,(uint32_t*)adcSampleBuffers[activeSampleBuffer], NUM_SAMPLES_PER_BUFFER);
}
