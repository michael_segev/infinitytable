#include "buttons.h"

uint32_t knobValues[3];

//old way of doing it
//void pollKnobValues(void)
//{
//	int i;
//	for(i = 0 ; i < 3 ; i++)
//	{
//		HAL_ADC_Start(&hadc1);
//		HAL_ADC_PollForConversion(&hadc1,5);
//		knobValues[i] = HAL_ADC_GetValue(&hadc1);
//	}
//	
//	HAL_ADC_Stop(&hadc1);	
//}

void setKnobValue(uint8_t knobIndex, uint32_t value)
{
	if(knobIndex > 2) return;
	knobValues[knobIndex] = value;	
}

//returns value from 0-100
float getKnobPosition(uint8_t idx)
{
	return (((float)knobValues[idx])*100.0f / 4095.0f);
}

bool getArcadeButtonValue(uint8_t idx)
{
	if(idx == 0)
	{
		return HAL_GPIO_ReadPin(BUTTON_1_GPIO_Port,BUTTON_1_Pin);
	}
	else
	{
		return HAL_GPIO_ReadPin(BUTTON_2_GPIO_Port,BUTTON_2_Pin);
	}
}