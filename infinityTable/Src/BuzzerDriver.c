#include "BuzzerDriver.h"

#define TIMERCLOCKSPEED 100000
#define TIMERMAXVALUE 0xFFF //12 bit timer

void initBuzzer(void)
{
	uint32_t coreClockFrequency = HAL_RCC_GetHCLKFreq();	
	
	htim1.Instance->PSC  = coreClockFrequency/TIMERCLOCKSPEED;	
}

void setBuzzerFrequency(uint32_t fhz)
{
	uint32_t coreClockFrequency = HAL_RCC_GetHCLKFreq();
	/*	
	float timePerClockTick = htim1.Instance->PSC/ coreClockFrequency;	
	float targetPeriod = 1 / fhz;	
	uint32_t numberOfTimerTicksPerTargetPeriod = targetPeriod/timePerClockTick;	
	htim1.Instance->ARR = numberOfTimerTicksPerTargetPeriod/2;
	*/
	
	//simplified math
	
	htim1.Instance->ARR = coreClockFrequency/(fhz*htim1.Instance->PSC);
	htim1.Instance->CCR1 = htim1.Instance->ARR >> 1;
	htim1.Instance->CNT = 0;
}

void turnOnBuzzer(void)
{
	HAL_TIM_PWM_Start(&htim1,TIM_CHANNEL_1);
}

void turnOffBuzzer(void)
{
	HAL_TIM_PWM_Stop(&htim1,TIM_CHANNEL_1);
}
