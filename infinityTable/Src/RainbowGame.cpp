#include "RainbowGame.hh"
#include "Sprite.hh"
#include "buttons.h"
#include <stdlib.h>
#include <vector>
#include "LedDriver.h" // to get num leds

RainbowGame::RainbowGame(void)
{
	
}

void RainbowGame::runGameEngine(void)
{
	float speed = (100 - getKnobPosition(2))/200.0f;
	//example game engine where all sprites are moved by 1
	for(int i = 0 ; i < SpritesVector.size(); i++)
	{
		float index = SpritesVector[i].getLocationIdx();
		index = index + speed;
		//out of bounds
		while(((int)index) > LedDriver_GetNumLEDS()-1) 
		{
			index = index - LedDriver_GetNumLEDS();
		}
		SpritesVector[i].setLocationIdx(index);
	}
	setGameInStrip();
}

void RainbowGame::startGame(void)
{
	//call parent start
	Game::startGame();	
	
	Sprite * redSprite 		=	new Sprite(3, 255, 0, 0, 0, 0, 0, false);
	Sprite * orangeSprite = new Sprite(3, 255, 100, 0, 3, 0, 0, false);
	Sprite * yellowSprite = new Sprite(3, 255, 255, 0, 6, 0, 0, false);
	Sprite * greenSprite 	=	new Sprite(3, 0, 255, 0, 9, 0, 0, false);
	Sprite * blueSprite 	=	new Sprite(3, 0, 0, 255, 12, 0, 0, false);
	Sprite * purpleSprite = new Sprite(3, 160, 32, 240, 15, 0, 0, false);
	
	redSprite->setOverflowMode(true);
	orangeSprite->setOverflowMode(true);
	yellowSprite->setOverflowMode(true);
	greenSprite->setOverflowMode(true);
	blueSprite->setOverflowMode(true);
	purpleSprite->setOverflowMode(true);
	
	Game::addSprite(*redSprite);
	Game::addSprite(*orangeSprite);
	Game::addSprite(*yellowSprite);
	Game::addSprite(*greenSprite);
	Game::addSprite(*blueSprite);
	Game::addSprite(*purpleSprite);

	
}

void RainbowGame::stopGame(void)
{
	//call parent stop
	Game::stopGame();	
	soundEngine.stopActiveSound(0);
}

