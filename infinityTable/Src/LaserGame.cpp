#include "LaserGame.hh"
#include "Sprite.hh"
#include "buttons.h"
#include <stdlib.h>
#include <vector>
#include "LedDriver.h" // to get num leds


//todo move color shit to seperate file
typedef struct Color
{
	uint8_t r;
	uint8_t g;
	uint8_t b;
}Color;
//interpolate between color 1 and color 2 ...t = range = 0->1
Color interpColor (Color a, Color b, float t)
{
	Color interpColor;
	interpColor.r = a.r + (float)(b.r - a.r) * t;
	interpColor.g = a.g + (float)(b.g - a.g) * t;
	interpColor.b = a.b + (float)(b.b - a.b) * t;
  //a.a + (b.a - a.a) * t	
	return interpColor;
}

LaserGame::LaserGame(void)
{
	
}

void LaserGame::runGameEngine(void)
{
	static bool prevButVals[2] = {true, true};
	bool butVals[2];
	uint16_t numPixels;
	
	butVals[0] = getArcadeButtonValue(0);
	butVals[1] = getArcadeButtonValue(1);
	
	if(butVals[0] == false && prevButVals[0] == true && SpritesVector.size() < 15) //press down
	{
		soundEngine.setActiveSound(293,20,0);
		numPixels = (rand() % LedDriver_GetNumLEDS()) + 1;
		float velocity = 0.5f + (0.05f*(rand()%20));
		addSprite(Sprite(numPixels, rand() % 255, rand() % 255, rand() % 255, -numPixels, 0, velocity, true)); //numpixels, r, g, b, locationIdx, priority, velocity, tailFade		
	}
	
	if(butVals[1] == false && prevButVals[1] == true && SpritesVector.size() < 15) //press down
	{
		soundEngine.setActiveSound(261,20,0);
		numPixels = (rand() % LedDriver_GetNumLEDS()) + 1;
		float velocity = 0.5f + (0.05f*(rand()%20));
		addSprite(Sprite(numPixels, rand() % 255, rand() % 255, rand() % 255, LedDriver_GetNumLEDS()-1, 0, -1.0f*velocity, true)); //numpixels, r, g, b, locationIdx, priority, velocity, tailFade		
	}
	
	prevButVals[0] = butVals[0];
	prevButVals[1] = butVals[1];	
	
	float speed = 0.2 + (100 - getKnobPosition(2))/50.0f;
	//move all sprites except wallpaper
	for(int i = 1 ; i < SpritesVector.size(); i++)
	{
		float id = SpritesVector[i].getLocationIdx();
		id = id + SpritesVector[i].getVelocity()*speed;
		//out of bounds
		if(id > LedDriver_GetNumLEDS() - 1 || id < 0.0f - SpritesVector[i].getNumPixels()) 
		{
			removeSprite(i);
			continue; //go to next for loop itteration
		}
		SpritesVector[i].setLocationIdx(id);
	}
	
	float knobColor = (100.0f-getKnobPosition(1))/100.0f;
	if(knobColor < 0.02) knobColor = 0;
	else if(knobColor > 1.0) knobColor = 1.0;
	Color a,b,walpaperColor;
	if(knobColor < 0.25)
	{
		a.r = 0;
		a.g = 0;
		a.b = 0;
		b.r = 0;
		b.g = 0;
		b.b = 255;	
		walpaperColor = interpColor(a,b,knobColor*4.0);
	}
	else if(knobColor < 0.5)
	{
		a.r = 0;
		a.g = 0;
		a.b = 255;
		b.r = 0;
		b.g = 255;
		b.b = 0;	
		walpaperColor = interpColor(a,b,(knobColor-0.25)*4.0);		
	}
	else if(knobColor < 0.75)
	{
		a.r = 0;
		a.g = 255;
		a.b = 0;
		b.r = 255;
		b.g = 0;
		b.b = 0;	
		walpaperColor = interpColor(a,b,(knobColor-0.5)*4.0);			
	}
	else if(knobColor <= 1.0)
	{
		a.r = 255;
		a.g = 0;
		a.b = 0;
		b.r = 255;
		b.g = 255;
		b.b = 255;	
		walpaperColor = interpColor(a,b,(knobColor-0.75)*4.0);					
	}	
	SpritesVector[0].setColor(walpaperColor.r,walpaperColor.g,walpaperColor.b);
	
	setGameInStrip();
}

void LaserGame::startGame(void)
{
	//call parent start
	Game::startGame();
	//Add Wallpaper
	addSprite(Sprite(LedDriver_GetNumLEDS(), 0, 0, 0, 0, 0, 0, false)); //numpixels, r, g, b, locationIdx, priority, velocity, tailFade		
}

void LaserGame::stopGame(void)
{
	//call parent stop
	Game::stopGame();	
	soundEngine.stopActiveSound(0);
}

