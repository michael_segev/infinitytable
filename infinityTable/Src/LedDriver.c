#include "LedDriver.h"
#include "stdbool.h"

#define NUMLEDS ((NUMLEDS_HORIZONTAL*2) + (NUMLEDS_VERTICAL*2))
#define NUMLEDS_HORIZONTAL 43
#define NUMLEDS_VERTICAL 17
#define MAX_CURRENT_MA 5000	//max allowed current in milliamps

//36 ticks -> 450ns
//68 ticks -> 850ns
//64 ticks -> 800ns
//32 ticks -> 400ns

#define NS_TO_TIM_TICKS(time_ns) ((time_ns*2)/25)
#define NS_TO_CPU_TICKS(time_ns) (8*time_ns)/100 //(HAL_RCC_GetHCLKFreq()*time_ns/1e-9)

#define FUDGE 24//24//10 //16

	 
//#define T0H 0 //32 - FUDGE
//#define T0L 68 - FUDGE
//#define T1H 64 - FUDGE
//#define T1L 36 - FUDGE


#define T0H NS_TO_TIM_TICKS(250)
#define T1H NS_TO_TIM_TICKS(1000) //no max!
#define T0L NS_TO_TIM_TICKS(1500) //max 5000
#define T1L T0L
#define TLL NS_TO_TIM_TICKS(50000) //latch low time

//CPU ticks minus 4 for gpio overhead
//#define T0H_CPU ((NS_TO_CPU_TICKS(250)/3) - 4)
//#define T1H_CPU ((NS_TO_CPU_TICKS(1000)/3) - 4) //no max!
//#define T0L_CPU (NS_TO_CPU_TICKS(1500)/3 - 4) //max 5000
//#define T1L_CPU T0L_CPU
//#define TLL_CPU (NS_TO_CPU_TICKS(50000)/3 - 4) //latch low time

#define T0H_CPU ((NS_TO_CPU_TICKS(125)/3) - 4)
#define T1H_CPU ((NS_TO_CPU_TICKS(800)/3) - 4) //no max!
#define T0L_CPU (NS_TO_CPU_TICKS(1500)/3 - 4) //max 5000
#define T1L_CPU 2
#define TLL_CPU (NS_TO_CPU_TICKS(100000)/3 - 4) //latch low time


//Private Variables
LEDpixel_tdef LEDpixel[NUMLEDS];
volatile bool enableTransmitLEDstripIT = false;
volatile uint32_t currentPixelIndex = 0;
volatile uint32_t currentBitIndex = 0;
volatile bool currentBitHigh = true;
uint16_t zeroIndexOffset = 0;

//Private functions
int16_t _LedDriver_OffsetIndex(int16_t appIndex);

void initLedStrip(void)
{
	
	HAL_GPIO_WritePin(GPIOA,GPIO_PIN_7,GPIO_PIN_RESET); //set data pin low
	HAL_Delay(1);
	//HAL_TIM_Base_Start_IT(&htim7); //start usecond timer ... 	
	HAL_TIM_Base_Start(&htim7); //start usecond timer ... 	
	clearPixels();
	sendPixelsToStrip(); //clear strip	
	HAL_Delay(1);
	
	LedDriver_SetZeroIndexOffset((NUMLEDS-1) - ((NUMLEDS_HORIZONTAL-1)/2)); //set index of bottom left corner of strip
	
	/*
	HAL_GPIO_WritePin(GPIOA,GPIO_PIN_7,GPIO_PIN_RESET); //set data pin low
	HAL_Delay(1);
	//HAL_TIM_Base_Start_IT(&htim7); //start usecond timer ... 	
	//HAL_TIM_Base_Start(&htim7); //start usecond timer ... 	
	clearPixels();
	sendPixelsToStrip_IT(); //clear strip	
	HAL_Delay(1000);	
	*/
}

#pragma push /* Save existing optimization level */
#pragma O0   /* Optimization level now O0 */
void sendRGBtoStrip(uint8_t r, uint8_t g, uint8_t b)
{
	int i,j;
	//green
	for (i = 7; i >= 0 ; i--)
	{
		if((g >> i) & 1) //1
		{
			//1		
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_7,GPIO_PIN_SET);
			for(j=0; j < T1H_CPU; j++)
			{
				__nop();
			}		
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_7,GPIO_PIN_RESET);
			for(j=0; j < T1L_CPU; j++)
			{
				__nop();
			}		
		}
		else //0
		{
			//0	
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_7,GPIO_PIN_SET);
			for(j=0; j < T0H_CPU; j++)
			{
				__nop();
			}		
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_7,GPIO_PIN_RESET);
			for(j=0; j < T0L_CPU; j++)
			{
				__nop();
			}		
		}
	}
	//red
	for (i = 7; i >= 0 ; i--)
	{
		if((r >> i) & 1) //1
		{
			//1		
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_7,GPIO_PIN_SET);
			for(j=0; j < T1H_CPU; j++)
			{
				__nop();
			}		
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_7,GPIO_PIN_RESET);
			for(j=0; j < T1L_CPU; j++)
			{
				__nop();
			}				
		}
		else //0
		{
			//0	
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_7,GPIO_PIN_SET);
			for(j=0; j < T0H_CPU; j++)
			{
				__nop();
			}		
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_7,GPIO_PIN_RESET);
			for(j=0; j < T0L_CPU; j++)
			{
				__nop();
			}		
		}
	}	
	//blue
	for (i = 7; i >= 0 ; i--) //skip last bit in loop
	{
		if((b >> i) & 1) //1
		{
			//1		
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_7,GPIO_PIN_SET);
			for(j=0; j < T1H_CPU; j++)
			{
				__nop();
			}		
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_7,GPIO_PIN_RESET);
			for(j=0; j < T1L_CPU; j++)
			{
				__nop();
			}		
		}
		else //0
		{
			//0	
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_7,GPIO_PIN_SET);
			for(j=0; j < T0H_CPU; j++)
			{
				__nop();
			}		
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_7,GPIO_PIN_RESET);
			for(j=0; j < T0L_CPU; j++)
			{
				__nop();
			}		
		}
	}	
}


void latchLedStrip(void)
{
//		htim7.Instance->CNT = 0;		
//		HAL_GPIO_WritePin(GPIOA,GPIO_PIN_7,GPIO_PIN_SET);
//		while(htim7.Instance->CNT < 60/*2*/)
//		{
//		}
	int i=0;
		htim7.Instance->CNT = 0;		
		HAL_GPIO_WritePin(GPIOA,GPIO_PIN_7,GPIO_PIN_RESET);
		for(i = 0 ; i < TLL_CPU; i++)
		{
			__nop();
		}
}
#pragma pop


void sendRGBtoStrip_timer(uint8_t r, uint8_t g, uint8_t b)
{
	int i;
	//green
	for (i = 7; i >= 0 ; i--)
	{
		if((g >> i) & 1) //1
		{
			//1		
			htim7.Instance->CNT = 0;
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_7,GPIO_PIN_SET);			
			while(htim7.Instance->CNT < T1H)
			{
			}
			htim7.Instance->CNT = 0;			
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_7,GPIO_PIN_RESET);
			while(htim7.Instance->CNT < T1L)
			{
			}	
		}
		else //0
		{
			//0	
			htim7.Instance->CNT = 0;			
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_7,GPIO_PIN_SET);		
			while(htim7.Instance->CNT < T0H)
			{
			}		
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_7,GPIO_PIN_RESET);	//putting write before cnt = 0 for first pixel that takes longer at start for some reason
			htim7.Instance->CNT = 0;			
			while(htim7.Instance->CNT < T0L)
			{
			}		
		}
	}
	//red
	for (i = 7; i >= 0 ; i--)
	{
		if((r >> i) & 1) //1
		{
			//1		
			htim7.Instance->CNT = 0;			
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_7,GPIO_PIN_SET);	
			while(htim7.Instance->CNT < T1H)
			{
			}		
			htim7.Instance->CNT = 0;			
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_7,GPIO_PIN_RESET);	
			while(htim7.Instance->CNT < T1L)
			{
			}				
		}
		else //0
		{
			//0	
			htim7.Instance->CNT = 0;			
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_7,GPIO_PIN_SET);			
			while(htim7.Instance->CNT < T0H)
			{
			}		
			htim7.Instance->CNT = 0;			
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_7,GPIO_PIN_RESET);		
			while(htim7.Instance->CNT < T0L)
			{
			}		
		}
	}	
	//blue
	for (i = 7; i >= 0 ; i--) //skip last bit in loop
	{
		if((b >> i) & 1) //1
		{
			//1		
			htim7.Instance->CNT = 0;			
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_7,GPIO_PIN_SET);
			while(htim7.Instance->CNT < T1H)
			{
			}		
			htim7.Instance->CNT = 0;			
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_7,GPIO_PIN_RESET);
			while(htim7.Instance->CNT < T1L)
			{
			}		
		}
		else //0
		{
			//0	
			htim7.Instance->CNT = 0;			
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_7,GPIO_PIN_SET);
			while(htim7.Instance->CNT < T0H)
			{
			}
			htim7.Instance->CNT = 0;			
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_7,GPIO_PIN_RESET);
			while(htim7.Instance->CNT < T0L)
			{
			}		
		}
	}	
}


void latchLedStrip_timer(void)
{
//		htim7.Instance->CNT = 0;		
//		HAL_GPIO_WritePin(GPIOA,GPIO_PIN_7,GPIO_PIN_SET);
//		while(htim7.Instance->CNT < 60/*2*/)
//		{
//		}
		htim7.Instance->CNT = 0;		
		HAL_GPIO_WritePin(GPIOA,GPIO_PIN_7,GPIO_PIN_RESET);
		while(htim7.Instance->CNT < TLL)
		{
		}
}



void sendPixelsToStrip(void)
{
	uint16_t i = 0;
	uint32_t totalPWM = 0;
	uint32_t totalCurrent_MA = 0; //in milliamps
	uint32_t percentTooBright;
	int j = 0;
	for(j = 0; j < NUMLEDS; j++)
	{
		totalPWM = totalPWM + (LEDpixel[j].r + LEDpixel[j].g + LEDpixel[j].b);
	}
	totalCurrent_MA = (20*totalPWM)/255;
	
	if(totalCurrent_MA > MAX_CURRENT_MA)
	{
		percentTooBright = (totalCurrent_MA*100)/MAX_CURRENT_MA;
		for(i = 0 ; i < NUMLEDS; i++)
		{
			LEDpixel[i].r = (((uint32_t)LEDpixel[i].r)*100)/percentTooBright;
			LEDpixel[i].g = (((uint32_t)LEDpixel[i].g)*100)/percentTooBright;
			LEDpixel[i].b = (((uint32_t)LEDpixel[i].b)*100)/percentTooBright;
		}		
	}
	
	__disable_irq();
	for(i = 0 ; i < NUMLEDS; i++)
	{
		sendRGBtoStrip(LEDpixel[i].r, LEDpixel[i].g, LEDpixel[i].b);
	}
	__enable_irq();	
	latchLedStrip();

}
/*
void sendPixelsToStrip_IT_Timer(void)
{
	uint8_t colorValue;
	uint8_t offset;
	if(currentBitIndex < 8) //green
	{
		offset = 0;
		colorValue = LEDpixel[currentPixelIndex].g;
	}
	else if(currentBitIndex < 16) //red
	{
		offset = 8;
		colorValue = LEDpixel[currentPixelIndex].r;
	}
	else if(currentBitIndex < 24) //blue
	{
		offset = 16;
		colorValue = LEDpixel[currentPixelIndex].b;
	}	
	
	if((colorValue >> (7 - currentBitIndex - offset)) & 1) //bit is 1
	{
		if(currentBitHigh)
		{
			currentBitHigh = false;
			htim7.Instance->ARR = T1L - 24;
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_7,GPIO_PIN_RESET);
		}
		else
		{
			currentBitHigh = true;
			htim7.Instance->ARR = T1H - 24;
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_7,GPIO_PIN_SET);			
		}
	}
	else //bit is 0
	{
		if(currentBitHigh)
		{
			currentBitHigh = false;
			htim7.Instance->ARR = T0L - 24;
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_7,GPIO_PIN_RESET);
		}
		else
		{
			currentBitHigh = true;
			htim7.Instance->ARR = T0H - 24;
			HAL_GPIO_WritePin(GPIOA,GPIO_PIN_7,GPIO_PIN_SET);			
		}	
	}
	
	currentBitIndex++;
	if(currentBitIndex > 23 && !currentBitHigh) //24 bits transmitted (both high and low)
	{
		currentBitIndex = 0;
		currentPixelIndex++;
	}
	
	if(currentPixelIndex < NUMLEDS - 1)
	{
		//HAL_TIM_Base_Start_IT(&htim7); //start usecond timer ...
	}
	else
	{
		HAL_TIM_Base_Stop_IT(&htim7); //stop usecond timer ...
		currentPixelIndex = 0;
		currentBitIndex = 0;
		enableTransmitLEDstripIT = false;
	}
}


//signals to timer to start
void sendPixelsToStrip_IT(void)
{
	if(!enableTransmitLEDstripIT) 
	{
		currentBitIndex = 0;
		currentPixelIndex = 0;
		HAL_TIM_Base_Stop_IT(&htim7); //stop usecond timer ... 
		htim7.Instance->CNT = 0;
		//figure out the first ARR (determined by MSB of first green pixel)
		if((LEDpixel[0].g >> 7) & 1)
		{
			htim7.Instance->ARR = T1H;			
		}
		else
		{
			htim7.Instance->ARR = T0H;		
		}
		currentBitIndex++;
		currentBitHigh = true;
		HAL_GPIO_WritePin(GPIOA,GPIO_PIN_7,GPIO_PIN_SET); //set data pin high
		enableTransmitLEDstripIT = true;
		HAL_TIM_Base_Start_IT(&htim7); //start usecond timer ... 			
	}
}
*/
uint8_t getPixelPriorityByIndex(int16_t idx)
{
	int16_t idxWithOffset;
	if(idx < 0 || idx > NUMLEDS -1)
	{
		return 0;
	}
	
	idxWithOffset = _LedDriver_OffsetIndex(idx); //correct for the strip offset
	
	if(idxWithOffset < 0 && idxWithOffset > NUMLEDS -1)
	{
		return 0;
	}
	return LEDpixel[idxWithOffset].priority;

}

LEDpixel_tdef getPixelByIndex(int16_t idx)
{
	LEDpixel_tdef blank;
	int16_t idxWithOffset;
	if(idx < 0 || idx > NUMLEDS -1)
	{
		return blank;
	}
	
	idxWithOffset = _LedDriver_OffsetIndex(idx); //correct for the strip offset
	
	if(idxWithOffset < 0 || idxWithOffset > NUMLEDS -1)
	{		
		return blank;
	}
	
	return LEDpixel[idxWithOffset];
}

void setPixelByIndex(uint8_t r, uint8_t g, uint8_t b, int16_t idx, uint8_t prio)
{
	int16_t idxWithOffset;
	if(idx < 0 || idx > NUMLEDS -1)
	{
		return;
	}
	
	idxWithOffset = _LedDriver_OffsetIndex(idx); //correct for the strip offset
	
	if(idxWithOffset < 0 || idxWithOffset > NUMLEDS -1)
	{
		return;
	}
	
	LEDpixel[idxWithOffset].r = r;
	LEDpixel[idxWithOffset].g = g;
	LEDpixel[idxWithOffset].b = b;
	LEDpixel[idxWithOffset].priority = prio;	
}

void clearPixels(void)
{
	int i;
	for(i = 0 ; i < NUMLEDS; i++)
	{
		LEDpixel[i].r = 0;
		LEDpixel[i].g = 0;
		LEDpixel[i].b = 0;		
		LEDpixel[i].priority = 0xFF; //reset priority of the pixel every frame
	}	
}

uint16_t LedDriver_GetNumLEDS(void)
{
	return NUMLEDS;
}

//sets the index which above layers refer to as zero
//the input offsetcorresponds to the LED number at the bottom left of the 
//table
void LedDriver_SetZeroIndexOffset(uint16_t offset)
{
	zeroIndexOffset = offset;
}

//takes app index and converts it to the strip index using
//the zero index offset
int16_t _LedDriver_OffsetIndex(int16_t appIndex)
{
		//not using modulus to improve runtime
		int16_t correctedIndex = (appIndex + zeroIndexOffset);
	
		while(correctedIndex >= NUMLEDS)
		{
			correctedIndex -= NUMLEDS;
		}	
	
		return correctedIndex;
}