#include "Game.hh"
#include "buttons.h"
#include "LedDriver.h" //to get num leds

Game::Game(void)
{
	
}

void Game::addSprite(Sprite spr)
{
	SpritesVector.push_back(spr);
}

void Game::removeSprite(uint32_t idx)
{
	SpritesVector.erase(SpritesVector.begin() + idx);
}

void Game::removeAllSprites(void)
{
	SpritesVector.clear();
}

void Game::addPixel(Pixel pixel)
{
	PixelsVector.push_back(pixel);
}

void Game::removePixel(uint32_t idx)
{
	PixelsVector.erase(PixelsVector.begin() + idx);
}

void Game::removeAllPixels(void)
{
	PixelsVector.clear();
}

void Game::setGameInStrip(void)
{
	for(int i = 0 ; i < SpritesVector.size(); i++)
	{
		SpritesVector[i].setSpriteInStrip();
	}
	for(int i = 0 ; i < PixelsVector.size(); i++)
	{
		PixelsVector[i].setPixelInStrip();
	}	
}

void Game::runGameEngine(void)
{
	//example game
	float speed = getKnobPosition(2)/10.0f;
	//example game engine where all sprites are moved by 1
	for(int i = 0 ; i < SpritesVector.size(); i++)
	{
		float id = SpritesVector[i].getLocationIdx();
		id = id + SpritesVector[i].getVelocity()*speed;
		//out of bounds
		if(id + SpritesVector.size() - 1  > LedDriver_GetNumLEDS() - 1 || id < 0) 
		{
			SpritesVector[i].setVelocity(SpritesVector[i].getVelocity()*-1.0f);
			id = id + SpritesVector[i].getVelocity()*speed;
		}
		SpritesVector[i].setLocationIdx(id);
	}
	
	setGameInStrip(); //must call this to set in strip
}

void Game::startGame(void)
{
		this->removeAllSprites();
	  this->removeAllPixels();
}

void Game::stopGame(void)
{
		this->removeAllSprites();
	  this->removeAllPixels();
}
