#include "SoundEngine.hh"
#include "BuzzerDriver.h"

SoundEngine::SoundEngine(void)
{
		activeSound = false;
		activeSoundFrequency = 0;
		activeSoundStartTimestamp = 0;
		activeSoundDurationMS = 0;
		activeSoundPriority = 0;
}

bool SoundEngine::setActiveSound(uint32_t frequency, int32_t durationMS, int8_t priority)
{
		if(activeSound && activeSoundPriority < priority)
		{
			return false; //error: active sound has higher priority
		}
		
		activeSoundFrequency = frequency;
		activeSoundStartTimestamp = HAL_GetTick();
		activeSoundDurationMS = durationMS;
		activeSoundPriority = priority;
		activeSound = true;
		pendingActiveSound = true;
		return true;
}

bool SoundEngine::stopActiveSound(int8_t priority)
{
		if(activeSoundPriority < priority)
		{
			return false;
		}
		activeSoundDurationMS = 0; //force active sound to stop
		return true;		
}

uint32_t SoundEngine::getActiveSoundFrequency(void)
{
	return activeSoundFrequency;
}
void SoundEngine::runEngine(void)
{
	//activate newly assigned sound
	if(pendingActiveSound)
	{
		pendingActiveSound = false;
		setBuzzerFrequency(activeSoundFrequency);
		turnOnBuzzer();		
	}
	//turn off active sound after duration
	if(activeSound && HAL_GetTick() > activeSoundStartTimestamp + activeSoundDurationMS)
	{
		activeSound = false;
		turnOffBuzzer();		
	}
}
