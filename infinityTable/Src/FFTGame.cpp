#include "FFTGame.hh"
#include "Sprite.hh"
#include "buttons.h"
#include <stdlib.h>
#include <vector>
#include "LedDriver.h" // to get num leds
#include "FFT.h"
#include <math.h>

FFTGame::FFTGame(void)
{
	gameStarted = false;
}

void FFTGame::startGame(void)
{	
  Pixel newPixel(0,0,0,0);
	
	Game::startGame(); //parent start game removes all sprites	
	gameStarted = true;
	for(int i = 0; i < LedDriver_GetNumLEDS(); i++)
	{
		newPixel.setLocationIdx(i);	
    addPixel(newPixel);		
	}	
}

void FFTGame::stopGame(void)
{
	Game::stopGame(); //parent stop game removes all sprites
	gameStarted = false;
}

#define CUTOFF_START 100
#define CUTOFF_LOW 400
#define CUTOFF_HIGH 3000
#define CUTOFF_MAX  6000
static float magnitudeSumsCutoff[3];
//static float magnitudeSumsCutoff_prev[3];
static float magnitudeSumsCutoff_LPF[3] = {0};
static float magnitudeSumsCutoff_SUPER_LPF[3] = {0};
static float color[3] = {0};
uint16_t numBinsPerRegion[3] = {0};
void FFTGame::runGameEngine(void)
{
	//int i,j,k;
	float * audioMagnitudes;
	
	for(int i = 0 ; i < 3; i++)
	{
		//magnitudeSumsCutoff_prev[i] = magnitudeSumsCutoff[i];
		magnitudeSumsCutoff[i] = 0;
		numBinsPerRegion[i] = 0;
	}

	FFTgetAudioMagnitudes(&audioMagnitudes);
	
	uint32_t frequency;
	for(int i = 0 ; i < FFT_MAGNITUDE_SIZE; i++)
	{
		frequency = i*BIN_FREQUENCY;
		if(frequency > CUTOFF_START && frequency <= CUTOFF_LOW)
		{
			magnitudeSumsCutoff[0] += audioMagnitudes[i];
			numBinsPerRegion[0]++;
		}
		else if(frequency > CUTOFF_LOW && frequency <= CUTOFF_HIGH)
		{
			magnitudeSumsCutoff[1] += audioMagnitudes[i];		
			numBinsPerRegion[1]++;			
		}
		else if(frequency > CUTOFF_HIGH && frequency <= CUTOFF_MAX)
		{
			magnitudeSumsCutoff[2] += audioMagnitudes[i];
			numBinsPerRegion[2]++;
		}
	}
	
	for(int i = 0 ; i < 3; i++)
	{
		magnitudeSumsCutoff[i] /= numBinsPerRegion[i]; //take average
		magnitudeSumsCutoff_LPF[i] = 0.9f*magnitudeSumsCutoff_LPF[i] + 0.1f*magnitudeSumsCutoff[i];
		magnitudeSumsCutoff_SUPER_LPF[i] = 0.995f*magnitudeSumsCutoff_SUPER_LPF[i] + 0.005f*magnitudeSumsCutoff[i];
		//magnitudeSumsCutoff_HPF[i] = magnitudeSumsCutoff[i] - magnitudeSumsCutoff_LPF[i];
	}
//	
//	
//	
//	color[0] = magnitudeSumsCutoff_HPF[0]/5000.0;
//	color[1] = magnitudeSumsCutoff_HPF[1]/5000.0;
//	color[2] = magnitudeSumsCutoff_HPF[2]/5000.0;	
	
	//color[0] = (magnitudeSumsCutoff_LPF[0] - 3300.0) / 3800.0;
	//color[1] = (magnitudeSumsCutoff_LPF[1] - 14000.0) / 8200.0;
	//color[2] = (magnitudeSumsCutoff_LPF[2] - 2300.0) / 1100.0;
	
	color[0] = (magnitudeSumsCutoff_LPF[0] - magnitudeSumsCutoff_SUPER_LPF[0]) / 800.0f;
	color[1] = (magnitudeSumsCutoff_LPF[1] - magnitudeSumsCutoff_SUPER_LPF[1]) / 2000.0f;
	color[2] = (magnitudeSumsCutoff_LPF[2] - magnitudeSumsCutoff_SUPER_LPF[2]) / 150.0f;	
		
	color[0] *= 255.0f;
	color[1] *= 255.0f;
	color[2] *= 255.0f;
	
	for (int i = 0; i < 3; i++)
	{
		if(color[i] > 255) color[i] = 255;
		else if(color[i] < 0) color[i] = 0;
	}

	for(int i = 0; i < PixelsVector.size(); i++)
	{
		//decrement all pixels every round
		uint8_t oldColor[3];
		PixelsVector[i].getColor(oldColor[0], oldColor[1], oldColor[2]);
		for(int j = 0 ; j < 3; j++)
		{
			if(oldColor[j] >= 6)
			{
				oldColor[j] -= 6;				
			}
			else
			{
				oldColor[j] = 0;
			}
		}
	  PixelsVector[i].setColor(oldColor[0],oldColor[1],oldColor[2]);
	}
	
	//make a random pixel according to new color
	static uint16_t pixelIndex = 0;
	
	if(color[0] /*+color[1]+color[2]*/ > 50)
	{
		pixelIndex++;
		if(pixelIndex >= PixelsVector.size()) 
		{ 
			pixelIndex = 0;		
		}
		PixelsVector[pixelIndex].setColor(color[0],color[1],color[2]);
	}
		
	Game::setGameInStrip();
}
