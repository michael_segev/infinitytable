#include "FFT.h"
#include "stm32l431xx.h"
#include "arm_math.h"


arm_rfft_fast_instance_f32 fftHandler; //fft handler for spectrum
float magnitudes[FFT_WINDOW_SIZE/2]; //stores the most recent magnitudes
	
void initFFT(void)
{
	arm_rfft_fast_init_f32(&fftHandler,FFT_WINDOW_SIZE); //init the FFT handler //all audio effects share the same fft handler	
}

void runFFT(float* inputSamples, float* outputMagnitudes)
{
	float freq[FFT_WINDOW_SIZE];

	arm_rfft_fast_f32(&fftHandler,inputSamples,freq,0); //FFT	
	//pitchDetectFrontBuffReady = false; //signals to back buffer filler that front buffer has finished being processed and should swap back buffer if full
	arm_cmplx_mag_f32(freq,outputMagnitudes,FFT_WINDOW_SIZE/2); //compute magnitudes ... num magnitudes = FFT_WINDOW_SIZE/2
}

//updates the magnitudes array with most recent audio data
void FFTcomputeAudioMagnitudes(float* inputSamples)
{
	runFFT(inputSamples, magnitudes); //updates the magnitudes array with most recent audio data	
}

void FFTgetAudioMagnitudes(float** outputMagnitudes)
{
	*outputMagnitudes = magnitudes;
}
