#include "Pixel.hh"
#include "LedDriver.h"

//Constructor 0
Pixel::Pixel(void)
{
	locationIdx = 0;
	red = 0;
	green = 0;
	blue = 0;
	priority = 0;
}
//Constructor 1
Pixel::Pixel(int16_t Idx)
{
	locationIdx = Idx;
	red = 0;
	green = 0;
	blue = 0;
	priority = 0;
}
//Constructor 2
Pixel::Pixel(int16_t Idx, uint8_t r, uint8_t g, uint8_t b)
{
	locationIdx = Idx;
	red = r;
	green = g;
	blue = b;
	priority = 0;
}

void Pixel::setColor(uint8_t r, uint8_t g, uint8_t b)
{
	red = r;
	green = g;
	blue = b;		
}

void Pixel::setLocationIdx(int16_t Idx)
{
	locationIdx = Idx;
}

void Pixel::getColor(uint8_t &r, uint8_t &g, uint8_t &b)
{
	r = red;
	g = green;
	b = blue;
}

uint8_t Pixel::getPriority(void)
{
	return priority;
}

void Pixel::setPriority(uint8_t prio)
{
	priority = prio;
}

void Pixel::setOverflowMode(bool value)
{
	this->overflowMode = value;
}

int16_t Pixel::getLocationIdx(void)
{
	return locationIdx;
}

void Pixel::setPixelInStrip(void)
{
	int16_t correctedLocationIdx;
	correctedLocationIdx = locationIdx;	
	if(overflowMode)
	{
		correctedLocationIdx = correctedLocationIdx % LedDriver_GetNumLEDS();
	}
	
	//check the active priority in current frame
	if(priority < getPixelPriorityByIndex(correctedLocationIdx))
	{
		//overlap existing pixel since this one is more important
		setPixelByIndex(red,green,blue,correctedLocationIdx, priority); //sets the pixel in the pixel array
	}
	else if(priority == getPixelPriorityByIndex(correctedLocationIdx))
	{
		//add in place if priorities are equal (merge colors)
		LEDpixel_tdef existingPixelInFrame = getPixelByIndex(correctedLocationIdx);
		uint16_t r = existingPixelInFrame.r + red;
		uint16_t g = existingPixelInFrame.g + green;
		uint16_t b = existingPixelInFrame.b + blue;
		if(r > 255) r = 255;
		if(g > 255) g = 255;
		if(b > 255) b = 255;
		setPixelByIndex(r,g,b,correctedLocationIdx, priority); //sets the pixel in the pixel array		
	}
	else
	{
		//don't update if target pixel has higher priority than this one
	}	
}
