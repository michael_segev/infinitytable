#include "Sprite.hh"
#include "LedDriver.h"

//Constructor 0
Sprite::Sprite(void)
{
	priority = 0;
	velocity = 0.0f;
	locationIdx = 0; //location on strip
	tailFade = false;
	overflowMode = false;
}

Sprite::Sprite(uint16_t numPixels)
{
	tailFade = false;
	this->addPixels(numPixels);
	priority = 0;
	velocity = 0.0f;
	locationIdx = 0; //location on strip
	overflowMode = false;
}

Sprite::Sprite(uint16_t numPixels, uint8_t r, uint8_t g, uint8_t b, int16_t locIdx, uint8_t prio, float velo)
{
	tailFade = false;
	this->addPixels(numPixels);
	this->setColor(r,g,b);
	this->setLocationIdx(locIdx);
	this->setPriority(prio);
	velocity = velo;	
	overflowMode = false;
}
Sprite::Sprite(uint16_t numPixels, uint8_t r, uint8_t g, uint8_t b, int16_t locIdx, uint8_t prio, float velo, bool tail)
{
	tailFade = tail;
	this->addPixels(numPixels);
	this->setLocationIdx(locIdx);
	this->setPriority(prio);
	velocity = velo;
	this->setColor(r,g,b);
	overflowMode = false;
}



void Sprite::addPixels(uint16_t numPixels)
{	
	for(int i = 0 ; i < numPixels; i++)
	{
		pixelsVector.push_back(Pixel(locationIdx+pixelsVector.size(),0,0,0));
		pixelsVector[pixelsVector.size() - 1].setPriority(priority);
	}	
}



//changes the color of all pixels in Sprite
void Sprite::setColor(uint8_t r, uint8_t g, uint8_t b)
{
	//update color of pixels
		uint32_t rtemp,gtemp,btemp;
		uint32_t sizeCubed = pixelsVector.size()*pixelsVector.size()*pixelsVector.size();
		for(int i = 0 ; i < pixelsVector.size(); i++)
		{
			if(tailFade && pixelsVector.size() > 1) //create tail fade effect (comet trail) as long as more than 1 pixel
			{
				if(velocity < 0)
				{									
					rtemp = (r*(pixelsVector.size() - i)*(pixelsVector.size() - i)*(pixelsVector.size() - i))/sizeCubed;
					gtemp = (g*(pixelsVector.size() - i)*(pixelsVector.size() - i)*(pixelsVector.size() - i))/sizeCubed;
					btemp = (b*(pixelsVector.size() - i)*(pixelsVector.size() - i)*(pixelsVector.size() - i))/sizeCubed;
//					rtemp = r - r*i/pixelsVector.size();
//					gtemp = g - g*i/pixelsVector.size();
//					btemp = b - b*i/pixelsVector.size();
				}
				else
				{					
					//segev fix this assymetry!
					rtemp = (r*i*i*i)/sizeCubed;
					gtemp = (g*i*i*i)/sizeCubed;
					btemp = (b*i*i*i)/sizeCubed;					
//					rtemp = r - r*(pixelsVector.size() - i)/pixelsVector.size();
//					gtemp = g - g*(pixelsVector.size() - i)/pixelsVector.size();
//					btemp = b - b*(pixelsVector.size() - i)/pixelsVector.size();
				}
			}
			else
			{
					rtemp = r;
					gtemp = g;
					btemp = b;			
			}
			
			pixelsVector[i].setColor(rtemp,gtemp,btemp);
		}
	
}

void Sprite::getColor(uint8_t &r, uint8_t &g, uint8_t &b)
{
	 pixelsVector[0].getColor(r,g,b);
}

void Sprite::setLocationIdx(float location)
{
	locationIdxFloat = location;
	locationIdx = location; //update internal variable
	//update vector of pixels
	for(int i = 0 ; i < pixelsVector.size(); i++)
	{
		pixelsVector[i].setLocationIdx(locationIdx + i);
	}
}

float Sprite::getLocationIdx(void)
{
	return locationIdxFloat;
}

float Sprite::getHeadLocationIdx(void)
{
	if(velocity >= 0) 
	{
		//for a sprite going forwards, location idx is actually the tail, so add the size minus 1
		return locationIdxFloat + (float)(getNumPixels() - 1);
	}
	else
	{
		//for a sprite going backwards head is equal to location
		return locationIdxFloat;
	}

}

void Sprite::setPriority(uint8_t prio)
{
	priority = prio;
	//update vector of pixels
	for(int i = 0 ; i < pixelsVector.size(); i++)
	{
		pixelsVector[i].setPriority(priority);
	}	
}

void Sprite::setVelocity(float velo)
{
	velocity = velo;
}
float Sprite::getVelocity(void)
{
	return velocity;
}

uint32_t Sprite::getNumPixels(void)
{
	return pixelsVector.size();
}

//set the sprites in the strip array
void Sprite::setSpriteInStrip(void)
{
	for(int i = 0 ; i < pixelsVector.size(); i++)
	{
		pixelsVector[i].setPixelInStrip();
	}
}

void Sprite::setOverflowMode(bool value)
{
	this->overflowMode = value;
	for(int i = 0 ; i < pixelsVector.size(); i++)
	{
		pixelsVector[i].setOverflowMode(value);
	}
}
